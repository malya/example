#!/usr/bin/env python3.7
"""AWS module tests."""

import os
import unittest
from unittest import mock
from datetime import datetime

import sbnp_faker.aws as aws
from sbnp_faker.unittest.mockups.files_mock import Files


DESTINATION = '/data/sbnp_faker'
KEYS = ['folder_1588063527/', 'folder_1588063527/file_1',
        'folder_1588063529/', 'folder_1588063529/file_1']

FILES = [Files(f) for f in KEYS]


def mocked_filter(Prefix=None):
    """Side effect for filter objects.

    :Parameters:
        - `Prefix`: prefix of key.

    :Return:
        List of file objects.
    """
    if Prefix:
        return [f for f in FILES if f.key.startswith(Prefix)]
    return FILES


class TestAWS(unittest.TestCase):

    """Class for test aws module."""

    def setUp(self):
        """Method for preparing the test fixture."""
        patcher = mock.patch('sbnp_faker.aws.resource')
        self.mocked_resource = patcher.start()
        self.addCleanup(patcher.stop)
        self.mocked_resource.return_value = mock.Mock()

        self.mocked_resource().Bucket().objects.all\
            .return_value = FILES
        self.mocked_resource().Bucket().objects.filter\
            .side_effect = mocked_filter

    @mock.patch('sbnp_faker.aws.os.mkdir')
    @mock.patch('sbnp_faker.aws.os.path.exists')
    def test_download_folder(self, mocked_exists, mocked_mkdir):
        """Test for sbnp_faker.aws.download_folder function.

        :Parameters:
            - `mocked_exists`: mock for os.path.exists.
            - `mocked_mkdir`: mock for os.mkdir.
        """
        for bool_value in [True, False]:
            mocked_exists.return_value = bool_value
            folder_path = aws.download_folder(KEYS[0], DESTINATION)
            self.assertEqual(folder_path, os.path.join(DESTINATION, KEYS[0]))

    @mock.patch('sbnp_faker.aws.shutil.rmtree')
    @mock.patch('sbnp_faker.aws.shutil.copytree')
    @mock.patch('sbnp_faker.aws.os.path.exists')
    def test_download_folder_qa(self, mocked_exists, mocked_copy, mocked_rm):
        """Test for sbnp_faker.aws.download_folder_qa function.

        :Parameters:
            - `mocked_exists`: mock for os.path.exists.
            - `mocked_copy`: mock for shutil.copytree.
            - `mocked_rm`: mock for shutil.rmtree.
        """
        for bool_value in [True, False]:
            mocked_exists.return_value = bool_value
            folder_path = aws.download_folder_qa(KEYS[0], DESTINATION)
            self.assertEqual(folder_path, os.path.join(DESTINATION, KEYS[0]))

    def test_get_list_of_folders(self):
        """Test for sbnp_faker.aws.get_list_of_folders function."""
        folders = [f.key for f in FILES if f.key.endswith('/')]
        aws_folders = [f.key for f in aws.get_list_of_folders()]
        self.assertEqual(aws_folders, folders)

    @mock.patch('sbnp_faker.aws.os.path.isdir')
    @mock.patch('sbnp_faker.aws.os.listdir')
    def test_get_list_of_folders_qa(self, mocked_listdir, mocked_isdir):
        """Test for sbnp_faker.aws.get_list_of_folders_qa function.

        :Parameters:
            - `mocked_listdir`: mock for os.listdir.
            - `mocked_isdir`: mcok for os.path.isdir.
        """
        FILES = ['folder_1588063527', 'file_1588063527']
        mocked_listdir.return_value = FILES
        mocked_isdir.side_effect = [True, False]
        folders = [f.key for f in aws.get_list_of_folders_qa()]
        self.assertEqual(folders, [FILES[0]])

# EOF
