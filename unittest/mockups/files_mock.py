#!/usr/bin/env python3.7
"""Module for Files mockup class."""

from datetime import datetime


class Files:

    """Mockup class for AWS S3 object."""

    def __init__(self, key):
        """Constructor of Files class.

        :Parameters:
            - `key`: AWS S3 object key.
        """
        self.key = key
        self.last_modified = datetime.fromtimestamp(int(key[7:17]))

# EOF
