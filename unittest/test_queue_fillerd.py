#!/usr/bin/env python3.7
"""QueueFillerD class tests."""

import time
import unittest
from unittest import mock

from sbnp_faker.bin.queue_fillerd import QueueFillerD
from sbnp_faker.unittest.mockups.files_mock import Files


OLD_KEYS = ['folder_1588063527/', 'folder_1588063529/']
KEYS = sorted(f'folder_{int(time.time())-i}/' for i in range(5))
FILES = [Files(f) for f in KEYS]


class TestQueueFillerD(unittest.TestCase):

    """QueueFillerD tests class."""

    def setUp(self):
        """Method for preparing the test fixture."""
        self.queue_fillerd = QueueFillerD()

    def test___init__(self):
        """Test __init__ method of QueueFillerD class."""
        self.assertIsInstance(self.queue_fillerd, QueueFillerD)

    @mock.patch("sbnp_faker.bin.queue_fillerd.time.sleep",
                side_effect=StopIteration)
    @mock.patch('sbnp_faker.rmq.add_rmq_items')
    @mock.patch('sbnp_faker.rmq.get_items_list')
    @mock.patch('sbnp_faker.aws.get_list_of_folders')
    def test_run(self, mocked_folders, mocked_rmq_items, mocked_rmq_add,
                 mocked_sleep):
        """Test for QueueFillerD run method.

        :Parameters:
            - `mocked_folders`: mock for aws.get_list_of_folders function.
            - `mocked_rmq_items`: mock for rmq.get_items_list function.
            - `mocked_rmq_add`: mock for rmq.add_rmq_items function.
            - `mocked_sleep`: mock for time.sleep method to stop infinite loop.
        """
        mocked_folders.return_value = FILES
        self.queue_fillerd._buffer = set(KEYS[3:5])
        mocked_rmq_items.return_value = KEYS[3:5]
        try:
            self.queue_fillerd.run()
        except StopIteration:
            self.assertEqual(self.queue_fillerd._buffer, set(KEYS))

    def test_get_last_modified(self):
        """Test for QueueFillerD get_last_modified method."""
        items = self.queue_fillerd.get_last_modified(FILES)
        self.assertEqual(items, set(KEYS))

        self.queue_fillerd._buffer = {KEYS[0]}
        items = self.queue_fillerd.get_last_modified(FILES)
        self.assertFalse(KEYS[0] in items)

    def test_last_modified_filter(self):
        """Test for QueueFillerD last_modified_filter method."""
        for f in FILES:
            self.assertTrue(self.queue_fillerd.last_modified_filter(f))

        old = [Files(f) for f in OLD_KEYS]
        for f in old:
            self.assertFalse(self.queue_fillerd.last_modified_filter(f))

# EOF
