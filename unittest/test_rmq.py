#!/usr/bin/env python3.7
"""RMQ module tests."""

import unittest
from unittest import mock

from sbnp_faker import rmq


BODY = ((mock.MagicMock(), mock.MagicMock(), '{"key": "folder_1588063529/"}'),
        (mock.MagicMock(), mock.MagicMock(), '{"key": "folder_1588063530/"}'),)

class TestRMQ(unittest.TestCase):

    """Class for rmq module tests."""

    def setUp(self):
        """Method for preparing the test fixture."""
        patcher = mock.patch('sbnp_faker.rmq.connection')
        self.mocked_connection = patcher.start()
        self.addCleanup(patcher.stop)
        self.mocked_connection.return_value = mock.MagicMock()
        self.mocked_channel = mock.MagicMock()
        self.mocked_channel.consume.return_value = BODY
        self.mocked_connection().channel.return_value.__enter__.return_value = self.mocked_channel

    def test_get_items_list(self):
        """Test rmq module get_items_list function."""
        self.assertEqual(rmq.get_items_list(), ['folder_1588063529/',
                                                'folder_1588063530/'])

    def test_consume(self):
        """Test rmq module consume function."""
        self.assertEqual(next(rmq.consume()), 'folder_1588063529/')

    def test_add_rmq_items(self):
        """Test rmq module add_rmq_items function."""
        self.assertEqual(rmq.add_rmq_items({'folder_1588063529/',
                                            'folder_1588063530/'}), None)

# EOF
