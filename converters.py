import os

from urllib.parse import urlparse

from ipaddress import IPv4Address, IPv6Address

from shared.data.tracker import TrackerHeader

import sbnp_faker.utils as utils
from sbnp_faker.clients.ipedia import get_sbrs_rules_by_ids


def connection(data):
    """ Convert CONNECTION section.

    Convert service logs into the following section of telemetry:

    :Parameters:
        - `data`: Service logs data for certain ESA.

    :Return:
        Converted CONNECTION section of SBNP telemetry.
    """
    ipedia_data, case_data, urs_data, sdr_data = [], [], [], []
    for item in data:
        ipedia_data.append(item['ipedia'])
        case_data.append(item['case'])
        urs_data.append(item['urs'])
        sdr_data.append(item['sdr'])

    result = {}
    for ipedia_obj in ipedia_data:
        # first check ipv4_addr for 0(zero), means 0.0.0.0 correct IP (wrong)
        ip_address = str(
            IPv4Address(ipedia_obj['request.endpoint'][0]['ipv4_addr'])
            if ipedia_obj['request.endpoint'][0]['ipv4_addr'] else
            IPv6Address(
                utils.ipv6_bytes(
                    ipedia_obj['request.endpoint'][0]['ipv6_addr'])))

        result[ip_address] = {}

        sbrs_rules = \
            get_sbrs_rules_by_ids(ipedia_obj['reply.result'][0]['rep_rule_id'],
                                  ipedia_obj['reply.rule_map_version'])
        if sbrs_rules:
            result[ip_address]['SBRS_RULES'] = sbrs_rules

        conn_guid_sdr_data = \
            [item for item in sdr_data \
             if ipedia_obj['connection_guid'] == item['connection_guid']]

        from_addrs = []
        for sdr_obj in conn_guid_sdr_data:
            result[ip_address]['HELO'] = \
                {sdr_obj['request.helo']: 1} if sdr_obj['request.helo'] else {}
            result[ip_address]['PTR'] = \
                sdr_obj['request.rdns'] if sdr_obj['request.rdns'] else ''
            from_addrs.extend(
                [item['addr'] for item in sdr_obj['request.headerFrom'] or []])

        # Match one Ipedia record to several Сase records
        conn_guid_case_data = \
            [item for item in case_data \
                if ipedia_obj['connection_guid'] == item['connection_guid']]
        for case_obj in conn_guid_case_data:
            fuzzy = result[ip_address].setdefault('FUZZY', {}) \
                        .setdefault('V9', {}) \
                        .setdefault(case_obj['case_fuzzy_hash'], {})
            fuzzy.update({
                'COUNT': 1, # TODO: ???
                'SCORE': 0, # TODO: ???
                'URIDB': {} # TODO: ???
            })

            froms = {}
            for addr in from_addrs:
                addr = utils.hash_from_addr(addr)
                froms.setdefault(addr, 0)
                froms[addr] += 1
            fuzzy['FROM'] = froms

            tos = {}
            for rcpt in case_obj['rcpt_to_fqdn']:
                rcpt = utils.hash_from_addr(rcpt)
                tos.setdefault(rcpt, 0)
                tos[rcpt] += 1
            fuzzy['TO'] = tos

            # Match one Case record to several Urs records
            ipas_urls = {}
            msg_guid_urs_data = \
                [item for item in urs_data \
                if case_obj['msg_guid'] == item['msg_guid']]
            for urs_obj in msg_guid_urs_data:
                for url in urs_obj['request.url']:
                    try:
                        url = urlparse(url['raw_url']).netloc
                        url = url.encode('ascii', 'ignore').decode('unicode_escape')
                    except ValueError as e:
                        continue
                    ipas_urls.setdefault(url, 0)
                    ipas_urls[url] += 1
            if ipas_urls:
                fuzzy['IPAS_URL'] = ipas_urls

            result[ip_address].setdefault('IPAS_MESSAGE_COUNT', 0)
            result[ip_address]['IPAS_MESSAGE_COUNT'] += 1

    return result


def ip(data):
    """ Convert IP section.

    Convert service logs into the following section of SBNP telemetry:

    :Parameters:
        - `data`: Service logs data for certain ESA.

    :Return:
        Converted IP section of SBNP telemetry.
    """
    ipedia_data, case_data = [], []
    for item in data:
        ipedia_data.append(item['ipedia'])
        case_data.append(item['case'])

    result = {}
    for ipedia_obj in ipedia_data:
        # first check ipv4_addr for 0(zero), means 0.0.0.0 correct IP (wrong)
        ip_address = str(
            IPv4Address(ipedia_obj['request.endpoint'][0]['ipv4_addr'])
            if ipedia_obj['request.endpoint'][0]['ipv4_addr'] else
            IPv6Address(
                utils.ipv6_bytes(
                    ipedia_obj['request.endpoint'][0]['ipv6_addr'])))

        conn_guid_case_data = \
            [item for item in case_data \
             if ipedia_obj['connection_guid'] == item['connection_guid']]
        for case_obj in conn_guid_case_data:
            message_count = result.setdefault(ip_address, {
                'CASE_SPAM_NEGATIVE': 0,
                'CASE_SPAM_SUSPECT': 0,
                'CASE_SPAM_POSITIVE': 0,
                'VOF_MESSAGE_COUNT': 0,
                'IPAS_MESSAGE_COUNT': 0,
                'SBNP_MESSAGE_COUNT': 0,
                'IPAS_SCORE_LOCAL': case_obj['ipas_score_local']})

            if not case_obj['ipas_results_string']:
                continue
            tracker = TrackerHeader(case_obj['ipas_results_string'])

            if tracker.ipas_score  < 50:
                message_count['CASE_SPAM_NEGATIVE'] += 1
            elif  50 <= tracker.ipas_score < 90:
                message_count['CASE_SPAM_SUSPECT'] += 1
            else:
                message_count['CASE_SPAM_POSITIVE'] += 1

            if tracker.vof_score > 0:
                message_count['VOF_MESSAGE_COUNT'] += 1

            message_count['IPAS_MESSAGE_COUNT'] += 1
            message_count['SBNP_MESSAGE_COUNT'] += 1

    return result


def sbrs(data):
    """ Convert SBRS section.

    Convert service logs into the following section of SBNP telemetry:

    :Parameters:
        - `data`: Service logs data for certain ESA.

    :Return:
        Converted SBRS section of SBNP telemetry.
    """
    ipedia_data = [item['ipedia'] for item in data]

    result = {}

    for obj in ipedia_data:
        for item in obj['reply.result']:
            if item['no_score']:
                score = result.setdefault('None', {})
            else:
                score = result.setdefault(str(item['reputation_x10'] / 10), {})

            sbrs_rules = \
                get_sbrs_rules_by_ids(item['rep_rule_id'],
                                      obj['reply.rule_map_version']) or '*'
            rules = score.setdefault(sbrs_rules, {})

            ips = rules.setdefault('I', [])
            ips.extend([hex(endpoint['ipv4_addr']) \
                        for endpoint in obj['request.endpoint']])

    return result


def ipas_rules_count(data):
    """ Convert IPAS_RULES_COUNT and IPAS_RULES_MESSAGE_COUNT sections.

    Convert service logs into the following section of SBNP telemetry:

    :Parameters:
        - `data`: Incomming data for certain ESA.

    :Return:
        Rule hits and count of CASE messages.
    """
    case_data = [item['case'] for item in data]

    rules, count = {}, 0
    for obj in case_data:
        if not obj['ipas_results_string']:
            continue
        tracker = TrackerHeader(obj['ipas_results_string'])
        ids = rules.setdefault(str(tracker.packages_version_int), {})
        ids['webint_enabled'] = int(tracker.webint_enabled)
        for rule_id in tracker:
            ids.setdefault(str(rule_id), 0)
            ids[str(rule_id)] += 1
        count += 1

    return rules, count


def av_verdicts(data):
    ipedia_data, esa_data, case_data = [], [], []
    for item in data:
        ipedia_data.append(item['ipedia'])
        esa_data.append(item['esa'])
        case_data.append(item['case'])

    av_verdicts = {
        1: 'AV_VIRAL',
        2: 'AV_REPAIRED',
        4: 'AV_CLEAN',
        8: 'AV_UNSCANNABLE',
        16: 'AV_ENCRYPTED',
        32: 'AMP_CVT'
    }

    result = {}
    for esa_obj in esa_data:
        verdict_name = av_verdicts.get(esa_obj['scanner_hits'])
        if not verdict_name:
            continue

        verdict = result.setdefault(verdict_name, {})
        attach_info = verdict.setdefault('ATTACH_INFO', {})
        ip_info = verdict.setdefault('IP', {})

        conn_guid_case_data = \
            [item for item in case_data \
             if esa_obj['connection_guid'] == item['connection_guid']]
        for case_obj in conn_guid_case_data:
            for attachment in case_obj['attachment_data']:
                try:
                    file_name = \
                        ''.join([chr(i) for i in attachment['file_name']])
                except Exception as e:
                    # TODO: file_name may contain negative numbers,
                    # that cannot be converted using chr()
                    continue

                file_name = utils.encode_filename(file_name)
                hashed_name = 'N{}:M{}:C{}'.format(file_name,
                                                   attachment['libmagic_type'],
                                                   attachment['mime_type'])
                attach_info.setdefault(hashed_name, 0)
                attach_info[hashed_name] += 1

                conn_guid_ipedia_data = \
                    [item for item in ipedia_data \
                    if case_obj['connection_guid'] == item['connection_guid']]
                for ipedia_obj in conn_guid_ipedia_data:
                    ip_address = str(
                        IPv4Address(ipedia_obj['request.endpoint'][0]['ipv4_addr'])
                        if ipedia_obj['request.endpoint'][0]['ipv4_addr'] else
                        IPv6Address(
                            utils.ipv6_bytes(
                                ipedia_obj['request.endpoint'][0]['ipv6_addr'])))

                    file_info = ip_info.setdefault(ip_address, {}) \
                                       .setdefault('FILE_INFO', {})

                    file_info_item = \
                        'E{}:M{}:C{}'.format(os.path.splitext(file_name)[1][1:],
                                             attachment['libmagic_type'],
                                             attachment['mime_type'])
                    file_info.setdefault(file_info_item, 0)
                    file_info[file_info_item] += 1

        if not attach_info:
            del result[verdict_name]

    return result


def sbnpv1(data):
    """ Convert sbnpv1 section.

    Convert service logs into the following section of SBNP telemetry:

    :Parameters:
        - `data`: Service logs data for certain ESA.

    :Return:
        Converted sbnpv1 section of SBNP telemetry.
    """
    ipedia_data, esa_data = [], []
    for item in data:
        ipedia_data.append(item['ipedia'])
        esa_data.append(item['esa'])

    result = {}
    for ipedia_obj in ipedia_data:
        ipv4_addr = str(ipedia_obj['request.endpoint'][0]['ipv4_addr'])
        if not ipv4_addr:
            continue

        ip_result = result.setdefault(ipv4_addr, {
            '1': 0,  # number of connections
            '2': 0,  # messages accepted
            '3': 0,  # invalid recipients
            '4': 0,  # brightmail positive messages
            '5': 0,  # brightmail "maybe spam" messages
            '6': 0,  # number of recipients
            '7': 0,  # aggregate message size
            '8': {}, # attachment extensions
            '9': 0,  # sophos positive messages
            '10': 0, # DKIM auth failed messages
            '11': 0, # SPF auth failed messages
            '12': 0  # DMARC verification failed messages
        })
        ip_result['1'] += 1

        conn_guid_esa_data = \
            [item for item in esa_data \
             if ipedia_obj['connection_guid'] == item['connection_guid']]
        for esa_obj in conn_guid_esa_data:
            ip_result['2'] += 1
            ip_result['3'] += esa_obj['invalid_rcpt_count']
            ip_result['6'] += esa_obj['rcpt_count']
            ip_result['7'] += esa_obj['msg_size']

    return result

# EOF
