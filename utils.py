import base64
import hashlib
import os
import re


def hash_from_addr(addr):
    # for the fuzzy hash return data we want to exclude + email addressing
    addr = re.sub(r'^([^+]+)\+[^@]+', r'\1', addr)
    return base64.b64encode(hashlib.md5(addr.encode()).digest())[0:4].decode()


def get_auth(serial):
    """Convert serial number to PhAuth string.

    :Parameters:
        - `serial`: device serial number
    :Return:
        PhAuth string
    """
    # Generate authentication string
    h = [10, 108, 69, 51, 104, 79, 102, 111, 62, 26, 90, 114, 46, 8, 63,
         125, 24, 125, 33, 86, 23, 22, 59, 94, 4, 70, 116, 121, 17, 89,
         41, 48, 33, 57, 10, 15, 17, 77, 19, 6, 14, 62, 5, 114, 66, 34,
         83, 101, 19, 111]

    result = hashlib.md5((serial + ''.join(map(chr, h))).encode()).hexdigest()
    return result


def _obfuscate(filename):
    """Takes a filename (minus extension) and returns the obfuscated version.
    Converts:
        a-z -> a
        A-Z -> A
        0-9 -> 0
        unprintable (ord in 128-255) -> x
        unprintable/unicode (ord > 255) -> X
    Also un-repr's any escaped unprintable characters, such as \\xff -> x,
    \\uffff -> X.  This is slightly hacky because filenames that *actually*
    have \\xff in the names would be translated to "x" instead of "\\aaa". """

    i = 0
    val = list(map(ord, list(filename)))
    while i < len(val):
        # XXX this code was needed because non-ascii characters were being
        # escaped.  Will delete completely if the fix works...
        #if val[i] == ord('\\') and i < len(val) - 3 and val[i+1] == ord('x'):
        #    val[i] = ord('x')
        #    val[i+1:i+4] = []
        #elif type(filename) == type(u'') and val[i] == ord('\\') and \
        #        i < len(val) - 5 and val[i+1] == ord('u'):
        #    val[i] = ord('X')
        #    val[i+1:i+6] = []
        if val[i] > 255:
            val[i] = ord('X')
        elif val[i] > 127:
            val[i] = ord('x')
        elif val[i] >= ord('a') and val[i] <= ord('z'):
            val[i] = ord('a')
        elif val[i] >= ord('A') and val[i] <= ord('Z'):
            val[i] = ord('A')
        elif val[i] >= ord('0') and val[i] <= ord('9'):
            val[i] = ord('0')
        i = i + 1

    return  ''.join(map(chr, val))


def encode_filename(filename):
    encoded_filename = filename.encode('ascii', 'xmlcharrefreplace')
    filename, extension = os.path.splitext(filename)
    encoded_filename, encoded_extension = os.path.splitext(encoded_filename)
    summer = hashlib.md5()
    summer.update(encoded_filename)
    filename_hash = summer.hexdigest()[0:12]
    extension = extension.encode('ascii','xmlcharrefreplace').decode()
    return 'h%s%s%s' % (filename_hash, _obfuscate(filename), extension)


def ipv6_bytes(address):
    return bytes(i % 256 for i in address)
# EOF
