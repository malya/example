#!/usr/bin/env python3.7

import argparse
import json
import logging
import os
import random
import shutil
import ssl
import time
import urllib.request

import bencode
from pyspark.sql import SparkSession
from shared.logging import configure_logging

import sbnp_faker.converters as converters
import sbnp_faker.utils as utils
from sbnp_faker.clients.kafka import KafkaProducer
from sbnp_faker.config import config
from sbnp_faker.constants import BLOCKED_ESA


configure_logging(disable_existing_loggers=False)
LOG = logging.getLogger('Converter')

# timestamps in service logs are represented as timestamp * 1000
TIME_FRACTION = 1000
# SBNP URL
SBNP_URL = '{}/{}'.format(config['ph_server.phalanx_host'],
                          config['ph_server.phalanx_endpoint'])

SEND_TO_KAFKA = config['processord.send_to_kafka']


class ConverterException(Exception):
    """Any exception during reading parquet files."""
    pass


def map_row(row):
    """Mapping function of Spark workers.

    :Parameters:
        - `row`: row passed by Spark map function.

    :Return:
        Modified representation of passed row. Contains only needed data.
    """
    return (
        (row.ipedia_request.app_info.device_id, row.esa_did_incoming_relay),
            {
                'case': {
                    'connection_guid': row.case_connection_guid,
                    'msg_guid': row.case_msg_guid,
                    'ipas_results_string':
                        ''.join([chr(i) for i in row.case_ipas_results_string or []]),
                    'ipas_score_local': row.case_ipas_score_local,
                    'case_fuzzy_hash':
                        ''.join([chr(i) for i in row.case_case_fuzzy_hash or []]),
                    'rcpt_to_fqdn': row.case_rcpt_to_fqdn,
                    'attachment_data': [] if not row.case_attachment_data else \
                        [obj.asDict() \
                         for obj in row.case_attachment_data or []],
                },
                'ipedia': {
                    'connection_guid': row.ipedia_connection_guid,
                    'reply.rule_map_version': row.ipedia_reply.rule_map_version,
                    'request.endpoint': [
                        {'ipv4_addr': obj.ipv4_addr,
                         'ipv6_addr': obj.ipv6_addr}
                        for obj in row.ipedia_request.endpoint],
                    'reply.result': [
                        {'rep_rule_id': obj.rep_rule_id,
                         'no_score': obj.no_score,
                         'reputation_x10': obj.reputation_x10}
                        for obj in row.ipedia_reply.result],
                },
                'esa': {
                    'connection_guid': row.esa_connection_guid,
                    'msg_guid': row.esa_msg_guid,
                    'scanner_hits': row.esa_scanner_hits,
                    'rcpt_count': row.esa_rcpt_count,
                    'invalid_rcpt_count': row.esa_invalid_rcpt_count,
                    'msg_size': row.esa_msg_size
                },
                'urs': {
                    'connection_guid': row.urs_connection_guid,
                    'msg_guid': row.urs_msg_guid,
                    'request.url': [] if not row.urs_request else \
                        [{'raw_url': obj.raw_url}
                         for obj in row.urs_request.url or []],
                },
                'sdr': {
                    'connection_guid': row.sdr_connection_guid,
                    'msg_guid': row.sdr_msg_guid,
                    'request.helo': row.sdr_request.helo \
                        if row.sdr_request else None,
                    'request.rdns': row.sdr_request.rdns \
                        if row.sdr_request else None,
                    'request.headerFrom': [] if not row.sdr_request else \
                        [{'addr': obj.addr} \
                         for obj in row.sdr_request.headerFrom or []],
                }
            }
        )


def convert(start_ts, end_ts, data):
    """Convert service logs into SBNP telemetry of certain period.

    :Parameters:
        - `start_ts`: start timestamp of the processed data.
        - `end_tsb`: end timestamp of the processed data.
        - `data`: service logs data.

    :Return:
        Bencoded representation like SBNP telemetry.
    """
    device_id_and_relay, device_data = data[0], [data[1]]
    device_id, did_incoming_relay = device_id_and_relay

    result = {device_id: {}}
    if did_incoming_relay is None:
        result[device_id]['UNKNOWN_INCOMING_RELAY'] = 1
    else:
        result[device_id]['DID_INCOMING_RELAY'] = int(did_incoming_relay)

    # CONNECTION section
    connection = converters.connection(device_data)
    if connection:
        result[device_id]['CONNECTION'] = connection

    # SL_IPAS_RULES_COUNT and SL_IPAS_RULES_MESSAGE_COUNT sections
    rules, count = converters.ipas_rules_count(device_data)
    if rules and count:
        result[device_id]['SL_IPAS_RULES_COUNT'] = rules
        result[device_id]['SL_IPAS_RULES_MESSAGE_COUNT'] = count

    # SBRS section
    sbrs = converters.sbrs(device_data)
    if sbrs:
        result[device_id]['SBRS'] = sbrs

    # IP section
    ip = converters.ip(device_data)
    if ip:
        result[device_id]['IP'] = ip

    # sbnpv1 section
    sbnpv1 = converters.sbnpv1(device_data)
    if sbnpv1:
        result[device_id]['sbnpv1'] = sbnpv1

    # AV_VERDICTS section
    av_verdicts = converters.av_verdicts(device_data)
    if av_verdicts:
        result[device_id].update(av_verdicts)

    result[device_id]['TIMESTAMP'] = int(time.time())
    result[device_id]['SBNP_FAKER'] = 1
    result[device_id]['SERIAL_NUMBER'] = {'2020111302': 2}

    if SEND_TO_KAFKA:
        return {utils.get_auth(device_id): result[device_id]}
    else:
        return {device_id: result[device_id]}


def merge_and_send(partition):
    partition = list(partition)
    result = {
        'DID_INCOMING_RELAY=1': [],
        'DID_INCOMING_RELAY=0': {},
        'UNKNOWN_INCOMING_RELAY=1': {}
    }
    device_ids = []
    for part in partition:
        for device_id, device_data in part.items():
            device_ids.append(device_id)

            relay = device_data.get('DID_INCOMING_RELAY')
            if relay is None:
                data_dict = result['UNKNOWN_INCOMING_RELAY=1']
            elif relay == 1:
                result['DID_INCOMING_RELAY=1'].append((device_id, device_data))
                continue
            else:
                data_dict = result['DID_INCOMING_RELAY=0']

            if not data_dict:
                data_dict.update(device_data)
            else:
                data_dict.setdefault('CONNECTION', {})
                for ip, ip_device_data in device_data.get('CONNECTION', {}).items():
                    if ip in data_dict['CONNECTION']:
                        for ip_key, ip_value in device_data['CONNECTION'][ip].items():
                            if ip_key == 'HELO':
                                helo_data = data_dict['CONNECTION'][ip].setdefault('HELO', {})
                                for helo_key, helo_value in ip_value.items():
                                    if helo_key in helo_data:
                                        helo_data[helo_key] += helo_value
                                    else:
                                        helo_data[helo_key] = helo_value

                            elif ip_key == 'IPAS_MESSAGE_COUNT':
                                data_dict['CONNECTION'][ip].setdefault('IPAS_MESSAGE_COUNT', 0)
                                data_dict['CONNECTION'][ip]['IPAS_MESSAGE_COUNT'] += ip_value

                            elif ip_key in ('PTR', 'SBRS_RULES'):
                                data_dict['CONNECTION'][ip].setdefault(ip_key, '')
                                data_dict['CONNECTION'][ip][ip_key] = ip_value

                            elif ip_key == 'FUZZY':
                                fuzzy_v9_data = data_dict['CONNECTION'][ip].setdefault('FUZZY', {}).setdefault('V9', {})
                                for fuzzy_key, fuzzy_value in ip_value['V9'].items():
                                    if fuzzy_key in fuzzy_v9_data:
                                        for key, value in fuzzy_value.items():
                                            if key == 'COUNT':
                                                fuzzy_v9_data[fuzzy_key].setdefault('COUNT', 0)
                                                fuzzy_v9_data[fuzzy_key]['COUNT'] += value
                                            elif key in ('FROM', 'IPAS_URL', 'TO', 'URIDB'):
                                                agg_data = fuzzy_v9_data[fuzzy_key].setdefault(key, {})
                                                for k, v in value.items():
                                                    if k in agg_data:
                                                        agg_data[k] += v
                                                    else:
                                                        agg_data[k] = v
                                            elif key == 'SCORE':
                                                fuzzy_v9_data[fuzzy_key].setdefault('SCORE', 0)
                                                fuzzy_v9_data[fuzzy_key]['SCORE'] = value
                                    else:
                                        fuzzy_v9_data[fuzzy_key] = fuzzy_value
                    else:
                        ip_data = data_dict['CONNECTION'].setdefault(ip, {})
                        ip_data = ip_device_data

                data_dict.setdefault('IP', {})
                for ip, ip_device_data in device_data.get('IP', {}).items():
                    if ip in data_dict['IP']:
                        data_dict['IP'][ip].setdefault('CASE_SPAM_NEGATIVE', 0)
                        data_dict['IP'][ip]['CASE_SPAM_NEGATIVE'] += ip_device_data['CASE_SPAM_NEGATIVE']
                        data_dict['IP'][ip].setdefault('CASE_SPAM_POSITIVE', 0)
                        data_dict['IP'][ip]['CASE_SPAM_POSITIVE'] += ip_device_data['CASE_SPAM_POSITIVE']
                        data_dict['IP'][ip].setdefault('CASE_SPAM_SUSPECT', 0)
                        data_dict['IP'][ip]['CASE_SPAM_SUSPECT'] += ip_device_data['CASE_SPAM_SUSPECT']
                        data_dict['IP'][ip].setdefault('IPAS_MESSAGE_COUNT', 0)
                        data_dict['IP'][ip]['IPAS_MESSAGE_COUNT'] += ip_device_data['IPAS_MESSAGE_COUNT']
                        data_dict['IP'][ip].setdefault('IPAS_SCORE_LOCAL', 0)
                        data_dict['IP'][ip]['IPAS_SCORE_LOCAL'] += ip_device_data['IPAS_SCORE_LOCAL']
                        data_dict['IP'][ip].setdefault('SBNP_MESSAGE_COUNT', 0)
                        data_dict['IP'][ip]['SBNP_MESSAGE_COUNT'] += ip_device_data['SBNP_MESSAGE_COUNT']
                        data_dict['IP'][ip].setdefault('VOF_MESSAGE_COUNT', 0)
                        data_dict['IP'][ip]['VOF_MESSAGE_COUNT'] += ip_device_data['VOF_MESSAGE_COUNT']
                    else:
                        data_dict['IP'][ip] = ip_device_data

                data_dict.setdefault('SL_IPAS_RULES_COUNT', {})
                for ip, ip_device_data in device_data.get('SL_IPAS_RULES_COUNT', {}).items():
                    if ip in data_dict['SL_IPAS_RULES_COUNT'] :
                        for rule, hits in ip_device_data.items():
                            data_dict['SL_IPAS_RULES_COUNT'][ip].setdefault(rule, 0)
                            if rule != 'webint_enabled':
                                data_dict['SL_IPAS_RULES_COUNT'][ip][rule] += hits
                    else:
                        data_dict['SL_IPAS_RULES_COUNT'][ip] = ip_device_data
                data_dict.setdefault('SL_IPAS_RULES_MESSAGE_COUNT', 0)
                data_dict['SL_IPAS_RULES_MESSAGE_COUNT'] += device_data.get('SL_IPAS_RULES_MESSAGE_COUNT', 0)

                data_dict.setdefault('SBRS', {})
                data_dict['SBRS'].update(device_data.get('SBRS', {}))
                for score, score_data in device_data.get('SBRS', {}).items():
                    if score in data_dict['SBRS']:
                        for rule, rule_data in score_data.items():
                            if rule in device_data['SBRS'][score]:
                                ips = data_dict['SBRS'][score].setdefault(rule, {}).setdefault('I', [])
                                ips.extend(device_data['SBRS'][score][rule]['I'])
                            else:
                                data_dict['SBRS'][score][rule] = rule_data

                    else:
                        data_dict['SBRS'][score] = score_data

                data_dict.setdefault('sbnpv1', {})
                data_dict['sbnpv1'].update(device_data.get('sbnpv1', {}))
                for ip, ip_device_data in device_data.get('sbnpv1', {}).items():
                    if ip in data_dict['sbnpv1'] :
                        for idx, idx_data in ip_device_data.items():
                            data_dict['sbnpv1'][ip].setdefault(idx, 0)
                            if isinstance(idx_data, int):
                                data_dict['sbnpv1'][ip][idx] += idx_data
                    else:
                        data_dict['sbnpv1'][ip] = ip_device_data

                if device_data.get('AV_VIRAL'):
                    data_dict.setdefault('AV_VIRAL', {}).setdefault('ATTACH_INFO', {})
                    data_dict['AV_VIRAL']['ATTACH_INFO'].update(device_data.get('AV_VIRAL', {}).get('ATTACH_INFO', {}))
                if device_data.get('AV_REPAIRED'):
                    data_dict.setdefault('AV_REPAIRED', {}).setdefault('ATTACH_INFO', {})
                    data_dict['AV_REPAIRED']['ATTACH_INFO'].update(device_data.get('AV_REPAIRED', {}).get('ATTACH_INFO', {}))
                if device_data.get('AV_CLEAN'):
                    data_dict.setdefault('AV_CLEAN', {}).setdefault('ATTACH_INFO', {})
                    data_dict['AV_CLEAN']['ATTACH_INFO'].update(device_data.get('AV_CLEAN', {}).get('ATTACH_INFO', {}))
                if device_data.get('AV_UNSCANNABLE'):
                    data_dict.setdefault('AV_UNSCANNABLE', {}).setdefault('ATTACH_INFO', {})
                    data_dict['AV_UNSCANNABLE']['ATTACH_INFO'].update(device_data.get('AV_UNSCANNABLE', {}).get('ATTACH_INFO', {}))
                if device_data.get('AV_ENCRYPTED'):
                    data_dict.setdefault('AV_ENCRYPTED', {}).setdefault('ATTACH_INFO', {})
                    data_dict['AV_ENCRYPTED']['ATTACH_INFO'].update(device_data.get('AV_ENCRYPTED', {}).get('ATTACH_INFO', {}))

    device_ids = list(set(device_ids) - set(BLOCKED_ESA))
    for relay, data in result.items():
        if isinstance(data, list): # DID_INCOMING_RELAY=1
            for device_id, device_data in data:
                device_data = [7, utils.get_auth(device_id), 3,
                               [int(time.time()), int(time.time()), device_data]]
                device_data = bencode.bencode(device_data)
                send_to_phlog(device_data)
        else:
            data = [7, utils.get_auth(random.choice(device_ids)), 3,
                    [int(time.time()), int(time.time()), data]]
            data = bencode.bencode(data)
            send_to_phlog(data)
    return partition


def send_to_phlog(data):
    req = urllib.request.Request(SBNP_URL, data=data.encode())
    urllib.request.urlopen(req, context=ssl.SSLContext())


def send_to_kafka(partition):
    partition = list(partition)
    kafka = KafkaProducer()
    for part in partition:
        kafka.write(json.dumps(part))
    kafka.producer.flush()
    return partition


def main(args):
    try:
        spark = SparkSession.builder.appName('Converter').getOrCreate()
        LOG.info('Reading parquet files from %s', args.source)
        df = spark.read.parquet(args.source)
    except Exception as e:
        LOG.exception('An error occurred during parquet files reading.')
        raise ConverterException(e)

    df.createOrReplaceTempView('data')
    start_ts, end_ts = spark.sql("""
        SELECT MIN(esa_msgTs), MAX(esa_msgTs) FROM data
    """).collect()[0]

    LOG.info('Converting data for %s - %s period...',
             start_ts // TIME_FRACTION, end_ts // TIME_FRACTION)

    rdd = spark.sql("""
        SELECT ipedia_guid AS ipedia_connection_guid,
               case_guid AS case_connection_guid,
               case_r_msg_guid AS case_msg_guid,
               esa_guid AS esa_connection_guid,
               esa_r_msg_guid AS esa_msg_guid,
               urs_guid AS urs_connection_guid,
               urs_r_msg_guid AS urs_msg_guid,
               sdr_guid AS sdr_connection_guid,
               sdr_msg_guid,
               ipedia_request,
               ipedia_reply,
               case_ipas_results_string,
               case_ipas_score_local,
               case_case_fuzzy_hash,
               case_rcpt_to_fqdn,
               case_attachment_data,
               esa_did_incoming_relay,
               esa_scanner_hits,
               esa_invalid_rcpt_count,
               esa_rcpt_count,
               esa_msg_size,
               urs_request,
               sdr_request
        FROM data
    """).dropDuplicates().rdd \
        .map(lambda row: map_row(row)) \
        .map(lambda data: convert(start_ts // TIME_FRACTION,
                                  end_ts // TIME_FRACTION,
                                  data))

    if SEND_TO_KAFKA:
        data_len = rdd.mapPartitions(send_to_kafka).count()
        LOG.info('%s records has been sent to Kafka.', data_len)
    else:
        data_len = rdd.mapPartitions(merge_and_send).count()
        LOG.info('%s records has been sent to Phalanx.', data_len)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Service logs to SBNP telemetry converter.')
    parser.add_argument('-s', '--source',
        default=config['processord.parquet_path'],
        help='Path to folder with parquet files.')
    args = parser.parse_args()

    main(args)

# EOF
