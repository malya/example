import atexit
import logging
import os
import re
import shutil
import time

from shared.logging import configure_logging

from sbnp_faker.clients import aws
from sbnp_faker.config import config

configure_logging(disable_existing_loggers=False)
LOG = logging.getLogger('Downloader')

UUID_RE = re.compile('-([a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12})\.')

DOWNLOADED_KEYS = []


def get_latest_aws_files():
    """Get latest files from AWS.

    :Return:
        A tuple: UUID and related files
    """
    LOG.info('Getting new files on AWS...')
    aws_items = list(aws.get_items())
    aws_items = [item for item in aws_items if '.part-' not in item.key]
    max_aws_item = max(aws_items, key=lambda item: item.last_modified)
    max_uuid = ''.join(re.search(UUID_RE, max_aws_item.key).groups())
    return max_uuid, [item.key for item in aws_items if max_uuid in item.key]


def run():
    """Main Downloader loop."""
    LOG.info('Downloader logic started.')
    while True:
        if len(os.listdir(config['processord.parquet_path'])) >= 2:
            LOG.info('There are too many unprocessed parquets. ' \
                     'Sleeping %s seconds...', config['processord.polling_time'])
            time.sleep(config['processord.polling_time'])
            continue

        key, files = get_latest_aws_files()
        if key in DOWNLOADED_KEYS:
            LOG.info('No new messages on AWS. Sleeping %s seconds...',
                     config['processord.polling_time'])
            time.sleep(config['processord.polling_time'])
            continue

        LOG.info('Got the next item %s to process.', key)
        download_folder = os.path.join(config['processord.download_path'], key)
        aws.download(files, download_folder)
        os.system('mv {} {}'.format(download_folder, config['processord.parquet_path']))
        DOWNLOADED_KEYS.append(key)
        LOG.info('Stored on %s', os.path.join(config['processord.parquet_path'], key))


if __name__ == '__main__':
    os.system('rm -rf {}/*'.format(config['processord.parquet_path']))
    atexit.register(LOG.info, 'Downloader stopped.')
    try:
        run()
    except Exception as err:
        LOG.exception('Abnormal exception occurred.')
        raise err

# EOF
