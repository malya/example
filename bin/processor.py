#!/usr/bin/env python3.7
"""Processor."""

import atexit
import logging
import os
import shutil
import subprocess
import time

from shared.logging import configure_logging

from sbnp_faker.config import config


configure_logging(disable_existing_loggers=False)
LOG = logging.getLogger('Processor')


def submit_spark_job(source):
    """Run spark job in separate process.

    :Parameters:
        - `source`: path to the downloaded parquet file(directory).

    :Return:
        Exit code of the command.
    """
    log_file = os.path.join(config['processord.spark_log_path'], 'current.log')
    cmd = '/usr/local/bin/spark-submit ' \
          '--master local[*] ' \
          '--properties-file spark_options.conf ' \
          '$PYSRCROOT/bin/converter.py -s {} 2> {}' \
          .format(source, log_file)

    p = subprocess.Popen(cmd,
                         stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         env=os.environ, shell=True)
    stdout, stderr = p.communicate()
    return p.returncode, stdout, stderr


def get_parquets_list(parquets_folder):
    parquets = [os.path.join(parquets_folder, d) \
                for d in os.listdir(config['processord.parquet_path'])]
    parquets.sort(key=lambda x: os.path.getmtime(x), reverse=True)
    return parquets


def run():
    """Main Processor loop."""
    LOG.info('Processor logic started.')
    while True:

        parquets = get_parquets_list(config['processord.parquet_path'])
        if not parquets:
            LOG.error('No parquets in the download folder. ' \
                      'Sleeping for %s seconds...',
                      config['processord.polling_time'])
            time.sleep(config['processord.polling_time'])
            continue

        LOG.info('Starting conversion process...')
        exit_code, stdout, stderr = submit_spark_job(parquets[0])
        if exit_code is not 0:
            log_file = os.path.join(config['processord.spark_log_path'],
                                    key+'.log')
            with open(log_file, 'wb') as fout:
                fout.write(stdout)

            LOG.error('Spark returned non zero exit code. ' \
                      'Retrying after %s seconds...',
                      config['processord.polling_time'])
            LOG.info('Error output has been stored in %s', log_file)
            time.sleep(config['processord.polling_time'])
            continue
        else:
            LOG.info('Conversion has been done.')

        LOG.info('Removing processed %s folder...', parquets[0])
        shutil.rmtree(parquets[0])


        LOG.info('Removing old log from %s ...',
                 config['processord.spark_log_path'])
        os.system(config['processord.spark_log_cleaner'])


if __name__ == '__main__':
    atexit.register(LOG.info, 'Processor logic stopped.')
    try:
        run()
    except Exception as err:
        LOG.exception('Abnormal exception occurred.')
        raise err

# EOF
