import ssl

import grpc

import ipd_pb2, service_ipd_pb2_grpc

from sbnp_faker.config import config


RULES_MAP = {'rules': {}, 'version': None}


def get_ipedia_client():
    """Get connection to Ipedia service."""
    with open('/opt/cert/ipedia/server.cert', 'rb') as fd:
        root_certificates = fd.read()

    with open('/opt/cert/ipedia/client.pm', 'rb') as fd:
        secret = fd.read()

    credentials = grpc.ssl_channel_credentials(
        root_certificates=root_certificates,
        private_key=secret,
        certificate_chain=secret)

    channel = grpc.intercept_channel(
        grpc.secure_channel('{}:{}'.format(config['ipedia.host'],
                                           config['ipedia.port']),
                            credentials))
    return channel


def get_sbrs_rules_by_ids(rule_ids, version):
    """Convert a list of SBRS rule IDs into their mnemonics.

    :Parameters:
        - `rule_ids`: a list of SBRS rule IDs.
        - `version`: an integer version of rule map.

    :Return:
        Converted string representation of SBRS rules.
    """
    if not RULES_MAP['rules'] or RULES_MAP['version'] < version:
        channel = get_ipedia_client()
        with channel:
            stub = service_ipd_pb2_grpc.IPDStub(channel)
            rules_map = stub.QueryRuleMap(ipd_pb2.RuleMapRequest())

            RULES_MAP['version'] = rules_map.version
            RULES_MAP['rules'] = {item.rep_rule_id: item.rule_mnemonic \
                                  for item in rules_map.rules}

    mnemonics = [RULES_MAP['rules'].get(_id, '??') for _id in rule_ids]
    mnemonics.sort()

    return ''.join(mnemonics)

# EOF


