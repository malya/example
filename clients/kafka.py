import logging

from confluent_kafka import Consumer, Producer, KafkaException

from sbnp_faker.config import config


class KafkaProducer:

    def __init__(self):
        self._log = logging.getLogger('kafka_writer')
        self.producer = Producer(self._generate_kafka_config(), logger=self._log)

    def write(self, data):
        """Read data from Kafka."""
        self.producer.produce(config['kafka.topics'], value=data)

    def _generate_kafka_config(self):
        """Generate Kafka config dict."""
        kafka_conf = {'security.protocol': 'ssl',
                      'bootstrap.servers': \
                        self._read_conf_file('/opt/kafka/kafka_bootstrap_servers')}
        if not config['kafka.official_cert']:
            kafka_conf['ssl.ca.location'] = '/opt/kafka/CARoot.pem'
            kafka_conf['ssl.certificate.location'] = '/opt/kafka/cert_filename.pem'
            kafka_conf['ssl.key.location'] = '/opt/kafka/key_filename.pem'
            kafka_conf['ssl.key.password'] = \
                        self._read_conf_file('/opt/kafka/ssl_client_key_password')
        else:
            kafka_conf['ssl.keystore.location'] = '/opt/kafka/ssl_client_key'
            kafka_conf['ssl.keystore.password'] = \
                               self._read_conf_file('/opt/kafka/ssl_client_key_password')
        return kafka_conf

    def _read_conf_file(self, fpath):
        """Read configuration file."""
        with open(fpath, 'r') as fd:
            return ''.join([x.strip() for x in fd])

# EOF
