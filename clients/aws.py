"""Module for AWS S3 storage management."""

import os
import shutil
import time
from datetime import datetime
from collections import namedtuple

import boto3

from sbnp_faker.config import config


def resource():
    """Initialize AWS S3 resource.

    :Return:
        Instance of AWS S3 resource.
    """
    return boto3.resource('s3', aws_access_key_id=config['aws.access_key'],
                                aws_secret_access_key=config['aws.secret_key'])



def download(files, destination):
    """Download files from S3 onto filesystem.

    :Parameters:
        - `files`: a list, keys as files on S3.
        - `destination`: folder path on filesystem where to store data.
    """
    if not os.path.exists(destination):
        os.makedirs(destination)

    bucket = resource().Bucket(config['aws.bucket'])
    for parquet in files:
        _, file_name = os.path.split(parquet)
        bucket.download_file(parquet, os.path.join(destination, file_name))


def download_qa(files, destination):
    """Download folder and its content onto filesystem (QA mode).

    :Parameters:
        - `files`: files as it is on AWS storage.
        - `destination`: path on filesystem where to store data.

    :Return:
        Folder path with stored data.
    """
    if not os.path.exists(destination):
        os.makedirs(destination)

    for parquet in files:
        shutil.copy(os.path.join(config['qa.bucket'], parquet), destination)


def get_items():
    """Get list of all items from S3 bucket.

    :Return:
        List of all items in the bucket.
    """
    bucket = resource().Bucket(config['aws.bucket'])
    prefix = config['aws.prefix'] + time.strftime('%Y%m%d', time.gmtime())
    objects = bucket.objects.filter(Prefix=prefix)
    return objects


def get_items_qa():
    """Get list of all items names on filesystem (QA mode).

    :Return:
        List of all items on the filesystem.
    """
    Item = namedtuple('Item', ['key', 'last_modified'])
    return [Item(os.path.join(os.path.basename(root), f), datetime.utcnow()) \
            for root, _, files in os.walk(config['qa.bucket']) if files \
            for f in files]


if config['qa.qa_mode']:
    download = download_qa
    get_items = get_items_qa

# EOF
